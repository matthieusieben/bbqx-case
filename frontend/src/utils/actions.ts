import type { ActionResult } from "backend/src/public-types"

export function processActionResults(results: ActionResult[]) {
  for (const result of results) {
    if (!result.succeeded)
      continue

    if (result.type === "popup") {
      const { message } = result.data
      alert(message)
    }
  }
}
