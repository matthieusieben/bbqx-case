import { ViteSSG } from "vite-ssg"
import generatedRoutes from "virtual:generated-pages"
import { setupLayouts } from "virtual:generated-layouts"
import App from "./App.vue"

import "@unocss/reset/tailwind.css"
import "./styles/main.css"
import "uno.css"

const routes = setupLayouts(generatedRoutes)

// https://github.com/antfu/vite-ssg
export const createApp = ViteSSG(App, { routes, base: import.meta.env.BASE_URL }, async (ctx) => {
  // install all modules under `modules/`
  const modules = import.meta.glob("./modules/*.ts", { eager: true })
  for (const name in modules) {
    const module = modules[name] as { install?: (ctx: any) => void }
    await module.install?.(ctx)
  }
})
