import { defineStore } from "pinia"
import type { ActionResult, Automation } from "backend/src/public-types"

export type Automations = Record<string, Automation>

const apiBaseURI = import.meta.env.VITE_BACKEND_URL_GITPOD ?? "http://localhost:8080"

export const useAutomationsStore = defineStore("automations", () => {
  const automations = ref<Automations | null>(null)

  /**
   * Load automations from the backend
   */
  async function loadAutomations({ force = !automations.value } = {}) {
    if (!force && automations.value !== null)
      return

    const response = await fetch(`${apiBaseURI}/automations`, {
      method: "GET",
      cache: force ? "no-cache" : undefined,
    })

    const { data } = await response.json()

    automations.value = data
  }

  async function createAutomation(data: Automation) {
    const response = await fetch(`${apiBaseURI}/automations`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })

    if (!response.ok) {
      throw new Error("Failed to create automation")
    }

    const json = (await response.json()) as { id: string; data: Automation }

    if (automations.value)
      automations.value[String(json.id)] = json.data
  }

  async function deleteAutomation(id: number | string) {
    const response = await fetch(`${apiBaseURI}/automations/${encodeURIComponent(id)}`, {
      method: "DELETE",
    })

    if (!response.ok) {
      throw new Error("Failed to delete automation")
    }

    if (automations.value)
      delete automations.value[id]
  }

  async function updateAutomation(id: number | string, data: Automation) {
    const response = await fetch(`${apiBaseURI}/automations/${encodeURIComponent(id)}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })

    if (!response.ok) {
      throw new Error("Failed to save automation")
    }

    const json = (await response.json()) as { data: Automation }

    if (automations.value)
      automations.value[id] = json.data
  }

  async function runAutomation(id: number | string) {
    const response = await fetch(`${apiBaseURI}/automations/run/${encodeURIComponent(id)}`, {
      method: "POST",
    })

    if (!response.ok) {
      throw new Error("Automation run failed")
    }

    const { data } = await response.json() as { data: ActionResult[] }

    return data
  }

  return {
    automations,
    automationsLoaded: computed(() => automations.value !== null),
    loadAutomations,
    deleteAutomation,
    createAutomation,
    updateAutomation,
    runAutomation,
  }
})
