import { defineConfig, presetUno } from "unocss"
import { presetTypography } from "@unocss/preset-typography"
import presetIcons from "@unocss/preset-icons"
import presetAttributify from "@unocss/preset-attributify"
import presetWebFonts from "@unocss/preset-web-fonts"
import transformerDirectives from "@unocss/transformer-directives"

export default defineConfig({
  shortcuts: [
    [
      "bbx-btn-primary",
      "flex items-center justify-center px-4 py-2 bg-[#3A8054] text-white cursor-pointer rounded-md transition hover:bg-[#2D6240] disabled:cursor-default disabled:opacity-50",
    ],
    [
      "bbx-btn-secondary",
      "flex items-center justify-center px-4 py-2 border border-[#E6E6E5] bg-white text-[#19181E] cursor-pointer rounded-md transition hover:bg-[#3A8054] hover:border-[#3A8054] hover:text-white disabled:cursor-default disabled:opacity-50",
    ],
    ["bbx-bg-primary", "bg-[#3A8054]"],
    ["bbx-bg-secondary", "bg-[#FCFAF7]"],
  ],
  transformers: [transformerDirectives()],
  presets: [
    presetUno(),
    presetAttributify(),
    presetIcons({
      scale: 1.2,
      warn: true,
    }),
    presetTypography(),
    presetWebFonts({
      fonts: {
        sans: [
          {
            name: "SF Text Pro",
            weights: ["100", "200", "300", "400", "500", "600", "700", "800"],
            italic: true,
          },
          {
            name: "sans-serif",
            provider: "none",
          },
        ],
        mono: [
          {
            name: "Inter",
            weights: ["100", "200", "300", "400", "500", "600", "700", "800"],
            italic: true,
          },
        ],
      },
    }),
  ],
  safelist: [
    "prose",
    "prose-sm",
    "bg-[#3A8054]",
    "bg-[#62B1EA]",
    "bg-[#B989F8]",
    "bg-[#E9F4FC]",
    "bg-[#EAF5EE]",
    "bg-[#F1E7FE]",
    "text-[#3A8054]",
    "text-[#62B1EA]",
    "text-[#B989F8]",
  ],
})
