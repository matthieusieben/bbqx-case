import { context } from "esbuild"
import serve from "@es-exec/esbuild-plugin-serve"
import { options } from "./esbuild.mjs"

(async () => {
  options.plugins.push(serve({
    main: "lib/index.js",
    verbose: true,
    runOnError: false,
    stopOnWarning: false,
  }))
  const ctx = await context(options).catch((e) => {
    console.error(e)
    throw e
  })
  await ctx.watch()
  console.log("Watching...")
})()
