import { buildServer } from "./server.js"

async function main() {
  const server = await buildServer()

  const PORT = Number(process.env.PORT) || 8080

  server.listen({ port: PORT, host: "0.0.0.0" }, (err, address) => {
    if (err)
      throw err

    server.log.info(`Server listening at ${address}`)
  })
}

void main()
