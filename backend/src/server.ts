import type { FastifyBaseLogger, FastifyTypeProvider, FastifyTypeProviderDefault, RawReplyDefaultExpression, RawRequestDefaultExpression, RawServerBase, RawServerDefault } from "fastify"
import { fastify } from "fastify"
import fastifyCors from "@fastify/cors"

import { type Database, createDatabase } from "./database.js"
import { createLogger } from "./logger.js"

import { automationsPlugin } from "./routes/automations.js"

export async function buildServer({ logger = createLogger() }: { logger?: FastifyBaseLogger } = {}) {
  const server = await fastify({ logger })

  server.setErrorHandler(async (error, request, reply) => {
    // Logging locally
    request.server.log.error(error)

    reply.status(500).send({ errorMsg: "Something went wrong", error })
  })

  // Enable the fastify CORS plugin
  server.register(fastifyCors, {
    origin: "*",
    methods: "GET,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
  })

  server.get("/", async (request, reply) => {
    reply.send({ app: "backend", date: new Date() })
  })

  // Database
  server.decorate("db", await createDatabase())

  // Routes
  server.register(automationsPlugin, { prefix: "/automations" })

  return server.withTypeProvider()
}

/**
 * Sadly, there is no proper way to type decorate() in fastify (this is actually due to a TS "limitation").
 * @see {@link https://github.com/fastify/fastify/issues/1417#issuecomment-458601746}
 */
declare module "fastify" {
  export interface FastifyInstance<
    RawServer extends RawServerBase = RawServerDefault,
    RawRequest extends RawRequestDefaultExpression<RawServer> = RawRequestDefaultExpression<RawServer>,
    RawReply extends RawReplyDefaultExpression<RawServer> = RawReplyDefaultExpression<RawServer>,
    Logger extends FastifyBaseLogger = FastifyBaseLogger,
    TypeProvider extends FastifyTypeProvider = FastifyTypeProviderDefault,
  > {
    db: Database
  }
}
