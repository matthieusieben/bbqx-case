import type { Action } from "./util/actions.js"

export interface Automation {
  name: string
  actions: readonly Action[]
}

export async function createDatabase() {
  // Fake DB for testing purposes
  return {
    automations: new Map<number, Automation>([
      [1, {
        name: "Automation 1",
        actions: [
          {
            type: "http",
            options: {
              url: "https://api.spacexdata.com/v4/payloads/5eb0e4b5b6c3bb0006eeb1e1",
            },
          },
          {
            type: "popup",
            options: {
              message: "Manufacturer of current payload : {{ steps[0].manufacturers }}",
            },
          },

        ],
      }],
    ]),
  }
}

export type Database = Awaited<ReturnType<typeof createDatabase>>
