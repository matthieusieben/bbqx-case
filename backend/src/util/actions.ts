import { badGateway, badRequest } from "@hapi/boom"

import { compileString } from "./json-template.js"
import type { JsonValue, Variables } from "./json-template.js"

type ActionRunnersDefinition = {
  [_ in string]?: (options: any, variables: Variables) => Promise<JsonValue>
}

const ACTION_RUNNERS = Object.freeze({
  // TODO: split in different files ?
  http: async (options: { url: string }, variables) => {
    const url = new URL(compileString(options.url, variables, true))
    const response = await fetch(url)
    if (!response.ok)
      throw badGateway("Bad response from server")

    const body: JsonValue = await response.json()
    return body
  },
  popup: async (options: { message: string }, variables) => {
    const message = compileString(options.message, variables, true)
    return { message }
  },
} satisfies ActionRunnersDefinition)

type ActionRunners = typeof ACTION_RUNNERS

export type ActionType = keyof ActionRunners
export type Action<T extends ActionType = ActionType> = {
  [K in T]: { type: K; options: Parameters<ActionRunners[K]>[0] }
}[T]

export type ActionReturn<T extends ActionType> = {
  [K in T]: Awaited<ReturnType<ActionRunners[K]>>
}[T]

interface ResultError { type: ActionType; succeeded: false; error: JsonValue }

type ResultSuccess<T extends ActionType = ActionType> = {
  [K in T]: { type: T; succeeded: true; data: ActionReturn<T> }
}[T]
export type ActionResult = { [K in ActionType]: ResultSuccess<K> }[ActionType] | ResultError

export async function runActions(actions: readonly Action[], { signal }: { signal?: AbortSignal } = {}) {
  const results: ActionResult[] = []
  for (const action of actions) {
    if (signal?.aborted)
      throw badRequest("Automation aborted")

    const { type } = action
    const variables = {
      steps: results.map(result => result.succeeded ? result.data : null),
    }

    try {
      const data = await runAction(action, variables)
      results.push({ type, succeeded: true, data } as any)
    } catch (err) {
      // TODO: error normalization
      const error = JSON.parse(JSON.stringify(err))
      results.push({ type, succeeded: false, error })

      // TODO: allow continuing on error ?
      return results
    }
  }

  return results
}

async function runAction(action: Action, variables: Variables) {
  try {
    const { type } = action
    const runner = ACTION_RUNNERS[type]
    return await runner(action.options as any, variables)
  } catch (err) {
    console.debug("Failed to run action", action, err)
    throw err
  }
}
