import { badData } from "@hapi/boom"
import JsonPath from "jsonpath"

export type JsonScalar = string | number | boolean | null
export type JsonValue = JsonScalar | Array<JsonValue> | { [k in string]?: JsonValue }
export type JsonObject = { [k in string]?: JsonValue }

export type Variables = Record<string, JsonValue>

export function compileValue<T extends JsonScalar>(input: T, variables: Variables): T
export function compileValue(input: unknown, variables: Variables): JsonValue
export function compileValue(input: unknown, variables: Variables): JsonValue {
  switch (typeof input) {
    case "number":
    case "boolean":
      return input
    case "string":
      return compileString(input, variables, false)
    case "object":
      if (input === null)
        return null
      if (Array.isArray(input))
        return input.map(v => compileValue(v, variables))
      else {
        const result: Record<string, JsonValue> = {}
        for (const [key, value] of Object.entries(input))
          result[key] = compileValue(value, variables)
        return result
      }
    default:
      throw badData("Expected scalar, array or object")
  }
}

export function compileString(value: string, variables: Variables, stringResult: true): string
export function compileString(value: string, variables: Variables, stringResult?: false): JsonValue
export function compileString(value: string, variables: Variables, stringResult = true): JsonValue {
  // Optimization: Avoid running rexeps
  if (!value.includes("{{"))
    return value

  if (!stringResult && /^\{\{\s*([^}\s]+)\s*\}\}$/.test(value)) {
    const path = value.slice(2, -2).trim()
    return getVariable(variables, path)
  }

  return value.replace(/\{\{\s*([^}\s]+)\s*\}\}/g, (_, path) => {
    const variable = getVariable(variables, path)
    return JSON.stringify(variable, path)
  })
}

export function getVariable(variables: Variables, path: string): JsonValue {
  const jsonPath = `$.${path}`
  return JsonPath.value(variables, jsonPath)
}
