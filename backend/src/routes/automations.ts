import { badRequest, notFound } from "@hapi/boom"
import type { FastifyPluginAsync, RouteOptions } from "fastify"

import type { Automation } from "../database.js"
import { runActions } from "../util/actions.js"

const list: RouteOptions = {
  method: "GET",
  url: "/",
  async handler(request, reply) {
    const data: Record<string, Automation> = {}

    for (const [id, automation] of this.db.automations)
      data[String(id)] = automation

    return reply.status(200).send({ status: "ok", data })
  },
}

const update: RouteOptions = {
  method: "POST",
  url: "/:id",
  // TODO: Validate body & params
  async handler(request, reply) {
    const id = Number((request.params as any)?.id)
    if (!id)
      throw badRequest("Invalid automation id")

    const data = request.body as Automation

    this.db.automations.set(id, data)

    return reply.status(200).send({ status: "ok", id, data })
  },
}

const create: RouteOptions = {
  method: "POST",
  url: "/",
  // TODO: Validate body & params
  async handler(request, reply) {
    const data = request.body as Automation

    // Find the next id
    let id = this.db.automations.size
    for (const v of this.db.automations.keys()) {
      if (v >= id)
        id = v + 1
    }

    this.db.automations.set(id, data)

    return reply.status(200).send({ status: "ok", id, data })
  },
}

const remove: RouteOptions = {
  method: "DELETE",
  url: "/:id",
  // TODO: Validate body & params
  async handler(request, reply) {
    const id = Number((request.params as any)?.id)
    if (!id)
      throw badRequest("Invalid automation id")

    this.db.automations.delete(id)

    return reply.status(200).send({ status: "ok" })
  },
}

const run: RouteOptions = {
  method: "POST",
  url: "/run/:id",
  // TODO: Validate body & params
  async handler(request, reply) {
    const id = Number((request.params as any)?.id)
    if (!id)
      throw badRequest("Invalid automation id")

    const automation = this.db.automations.get(id)
    if (!automation)
      throw notFound("Automation not found")

    const data = await runActions(automation.actions)

    return reply.status(200).send({ status: "ok", data })
  },
}

export const automationsPlugin: FastifyPluginAsync = async (server) => {
  server.route(list)
  server.route(create)
  server.route(update)
  server.route(remove)
  server.route(run)
}
